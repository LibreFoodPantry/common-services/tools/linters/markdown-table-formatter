#!/usr/bin/env bash

set -e

mkdir -p ./artifacts/release
echo "# Build ${1}" >> ./artifacts/release/markdown-table-formatter.yml
cat ./source/gitlab-ci/markdown-table-formatter.yml >> ./artifacts/release/markdown-table-formatter.yml

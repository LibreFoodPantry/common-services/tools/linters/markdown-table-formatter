# markdown-table-formatter

Checks for style in markdown tables. Does not update markdown - only checks.

To enable markdown-table-formatter in your project's pipeline add
`markdown-table-formatter` to the `ENABLE-JOBS`
variable in your project's `.gitlab-ci.yml` e.g.:

```bash
variables:
  ENABLE_JOBS: "markdown-table-formatter"
```

This is a thin wrapper around
[markdown-table-formatter](https://www.npmjs.com/package/markdown-table-formatter).

For more detailed configuration and usage instructions, see
[the documentation](https://www.npmjs.com/package/markdown-table-formatter).

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
